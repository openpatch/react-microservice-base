if [ "$NODE_ENV" = "production" ]; then \
  yarn install --no-cache --frozen-lockfile --production; \
elif [ "$NODE_ENV" = "test" ]; then \
  yarn install --no-cache --frozen-lockfile; \
else
  yarn install
fi;
exec "$@"
