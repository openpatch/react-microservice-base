FROM node:12-alpine

ENV NODE_PATH=/node_modules
ENV PATH=$PATH:/node_modules/.bin

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

COPY entrypoint.sh /usr/src/entrypoint.sh

# http port
EXPOSE 3000
# Websocket port
EXPOSE 35729

ENTRYPOINT ["sh", "/usr/src/entrypoint.sh"]

CMD ["yarn", "start"]
